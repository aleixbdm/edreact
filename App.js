import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity } from 'react-native';
import { sprintf } from 'sprintf-js';
import moment from 'moment';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

const data = [
        {
            image: 'https://images.unsplash.com/photo-1429681601148-75510b2cef43?auto=compress,format&fit=crop&w=640&h=480&q=60',
            location: 'Café Kahvipuu, Kokkola',
            author: 'Seemi Samuel',
            likes: 8,
            dateTime: '2017-12-11T00:00:00'
        },
        {
            image: 'https://images.unsplash.com/photo-1453614512568-c4024d13c247?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
            location: 'Bintaro, Indonesia',
            author: 'Nafinia Putra',
            likes: 43,
            dateTime: '2015-07-18T00:00:00'
        },
        {
            image: 'https://images.unsplash.com/photo-1489396944453-834c536105c0?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=60',
            location: 'Bodø, Bodo, Norway',
            author: 'Thomas Litangen',
            likes: 17,
            dateTime: '2010-10-01T00:00:00'
        },
        {
            image: 'https://images.unsplash.com/5/unsplash-bonus.jpg?auto=compress,format&fit=crop&w=640&h=480&q=60',
            location: 'Kitsuné Café, Montreal',
            author: 'Luke Chesser',
            likes: 3,
            dateTime: '2007-02-22T00:00:00'
        },
        {
            image: 'https://images.unsplash.com/photo-1441336558687-a06957a7642a?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
            location: 'Iconoclast Koffiehuis, Edmonton',
            author: 'Redd Angelo',
            likes: 6,
            dateTime: '2013-05-10T00:00:00'
        }
    ];

class CoffeBar extends Component {
    state = {
        likes: this.props.likes
    };

    addLike = () => {
        this.setState({ likes: this.state.likes + 1 }, () =>{
            console.log(sprintf('Like is %1$s', this.state.likes));
        });
    }

    render() {

		let dateFormated = moment(this.props.dateTime).format('DD-MM-YYYY');

        return (
            <View style={styles.wrapper}>
                <Image style={styles.image} source={{ uri: this.props.image }} />
                <Text style={styles.likes}> {this.state.likes} </Text>
                <View style={styles.bottomWrapper}>
                    <Text style={styles.location}>{this.props.location}</Text>
                    <Text style={styles.author}>by {this.props.author} on {dateFormated}</Text>
                    {this.state.likes < 10 && (
                        <TouchableOpacity style={styles.button} onPress={this.addLike}>
                            <Text style={styles.buttonText}>
                            	<Icon style={styles.buttonIcon} name="like"/> Like
                            </Text>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
        );
    }
}

export default class App extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={styles.scrollContent}>
                {data.map((entry, idx) => {
                    return (
                        <CoffeBar 
                            key={idx}
                            {...entry}
                        />
                    );
                })}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    wrapper: {
        top: 20
    },
    scrollContent: {
        paddingTop: 20
    },
    bottomWrapper: {
        position: 'absolute',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#0009',
        width: '100%',
        bottom: 0
    },
    image: {
        width: '100%',
        height: 250
    },
    likes: {
        position: 'absolute',
        color: '#fff',
        fontSize: 20,
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 15,
        overflow: 'hidden',
        textAlign: 'center',
        minWidth: 30,
        height: 30,
        top: 10,
        right: 10,
        backgroundColor: '#0009'
    },
    author: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold'
    },
    location: {
        color: '#fff',
        fontSize: 16
    },
    button: {
        position: 'absolute',
        right: 10,
        height: 35,
        width: 70,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#3B5998',
        borderRadius: 5
    },
    buttonIcon: {
        color: '#fff',
        fontSize: 16
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
});