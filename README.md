# Entrega Entorno de desarrollo React

Aplicación desarrollada en React. Funciona tanto para Android como iOS. Testeada con un emulador nexus 5X api 25 en la app Android, y con un simulador iphone SE 11.2 para la app iOS.

Se ha instalado el módulo puro js de moment.js y el módulo nativo react-native-vector-icons.

Una vez esto se ha modificado el App.js para utilizar importar los módulos con:

import moment from 'moment';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

Entonces, se ha podido formatar la fecha usando moment con:

let dateFormated = moment(this.props.dateTime).format('DD-MM-YYYY');

y luego usar este parámetro para mostrarlo por pantalla.

Finalmente, para usar el icono de like se ha añadido la siguiente línea:

<Icon style={styles.buttonIcon} name="like"/>

El ejercicio se ha realizado siguiendo el ejemplo realizado en clase. Las funcionalidades siguen igual y el Like se ha formatado como se pedía en el ejercicio.

Para poder ejecutar el proyecto se han seguido los siguentes pasos:

1) Primero en la carpeta ejecutar el comando react-native -v. Tendria que salir por consola:

react-native-cli: 2.0.1
react-native: 0.51.0

En el caso que se mostrase react-native: n/a - not inside a React Native project directory, se ha ejecutado:

npm update

2) Para ejecutar en iOS:

react-native run-ios

3) Para ejecutar en Android:

react-native run-android

Tener en cuenta que tenemos que tener en la carpeta android un archivo local.properties con el path del android sdk.

4) En caso de dar error al abrir la app, se ha ejecutado:

watchman watch-del-all
rm -rf node_modules && npm install
npm start -- --reset-cache
rm -fr $TMPDIR/react-*
react-native run-ios